var editor = new Quill('#editor', {
    modules: {
        'toolbar': [
            [{ 'font': [] }, { 'size': [] }],
            ['bold', 'italic', 'underline', 'strike']
        ]
    },
    theme: 'snow'
});

function cargarDelta(){
    let contenido = editor.getContents();
    let contenidoString = JSON.stringify(contenido).toString();
    document.getElementById("contenido").value = contenidoString;

    //let example  = JSON.parse(contenidoString);

    //editor.setContents(example);
    //console.log(editor.setContents(example));

}
