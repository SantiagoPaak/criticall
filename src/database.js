const mysql = require('mysql2');
const { database } = require("./keys");

const pool = mysql.createPool(database);
//promise-pool solo es el comunicador con mysql
//alternativa, usar mysql2

pool.getConnection((err, connection) => {
    if(err) {
        if(err.code === 'PROTOCOL_CONNECTION_LOST'){
            console.error('DATABASE CONNECTION WAS CLOSED');
        }
        if (err.code === 'ER_CON_COUNT_ERROR'){
            console.err('DATABASE HAS TOO MANY CONNECTIONS');
        }
        if (err.code === 'ECONNREFUSED'){
            console.error('DATABASE CONNECTION WAS REFUSED');
        }
    } 
     else if (connection){
        pool.releaseConnection(connection);
        console.log('DB is Connected');
    }  
    return;
    
});

module.exports = pool;
