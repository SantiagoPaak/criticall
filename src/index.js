const app = require('./server')


//Starting the server
app.listen(app.get('port'), ()=>{
    console.log("Starting server on port", app.get('port'));
});
