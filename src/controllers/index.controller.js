const pool = require('../database');

const indexCtrl = {};

indexCtrl.renderIndex = ( req, res ) => {
    res.render('partials/index');
};

indexCtrl.renderLogin = ( req, res ) => {
        res.render('partials/form-user');
};

indexCtrl.renderArticle = async ( req, res ) => {
    //Este codigo es arte CRACK
    const { id }      = req.params; 
    const promisePool = pool.promise();
    const tagsSQL     = await promisePool.query('SELECT * FROM tags JOIN tags_article ON tags.id = tags_article.tags_id WHERE tags_article.articulos_id = 1');
    const articuloSQL = await promisePool.query('SELECT articulos.titulo, articulos.cuerpo, articulos.fecha_creacion, categorias.categoria FROM articulos INNER JOIN categorias ON categorias.id=articulos.id_categoria WHERE articulos.id=' + id);

    //Destructuring del articulo
    const { titulo, cuerpo, fecha_creacion, categoria } = articuloSQL[0][0];
    const articuloObj = {
        titulo,
        cuerpo,
        fecha_creacion,
        categoria 
    };

    //Destructuring de todos los tags obtenidos
    const tagsTotales = [];

    tagsSQL[0].forEach(tagSQL => {
        const { tag, descripcion } = tagSQL;
        tagsTotales.push({
            tag: tag,
            descripcion: descripcion
        }); 
    });
    //Enviamos al render de articulo los parametros necesarios
    res.render('partials/article',{articulos: articuloObj, tags: tagsTotales });
};

indexCtrl.createUser = async ( req, res ) => {
    const { name, surname, nick, email, password, image, description } = req.body;
    let id_privilegio = 1
    let fecha = new Date();
    const newUser = {
        name,
        surname,
        nick,
        email,
        password,
        image,
        id_privilegio,
        description,
    }
       await pool.query(`INSERT INTO usuarios (nombre, apellido, nick, email, password, image_route, id_privilegio, descripcion, fecha_creacion) VALUES ('${newUser.name}', '${newUser.surname}', '${newUser.nick}', '${newUser.email}', '${newUser.password}', '${newUser.image}', '1', '${newUser.description}', '2020-11-09 20:46:56')`);
       res.send("Receiver");
   };

indexCtrl.renderEditor = (req,res) => {
    res.render("partials/editor",{ title: 'Editor de texto',layout: 'editor' });
};


module.exports = indexCtrl;